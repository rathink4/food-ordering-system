package OOPs;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class FoodPanel extends JPanel {

    public FoodPanel(){
        Dimension dim = getPreferredSize();
        dim.width = 270;
        setPreferredSize(dim);

        Border innerBorder = BorderFactory.createTitledBorder("Food");
        Border outerBorder = BorderFactory.createLoweredSoftBevelBorder();
        setBorder(BorderFactory.createCompoundBorder(outerBorder,innerBorder));
    }
}
