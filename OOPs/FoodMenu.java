package OOPs;

import javax.swing.*;
import java.awt.*;

public class FoodMenu extends JFrame {

    private FoodPanel foodPanel;
    private DessertPanel dessertPanel;

    public FoodMenu(){
        super("Food Menu");

        foodPanel = new FoodPanel();
        dessertPanel = new DessertPanel();

        add(foodPanel, BorderLayout.WEST);
        add(dessertPanel, BorderLayout.EAST);

        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900,900);
        setLocationRelativeTo(null);

    }
}
