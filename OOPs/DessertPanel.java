package OOPs;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class DessertPanel extends JPanel {
    public DessertPanel(){
        Dimension dim = getPreferredSize();
        dim.width = 270;
        setPreferredSize(dim);

        Border innerBorder = BorderFactory.createTitledBorder("Dessert");
        Border outerBorder = BorderFactory.createLoweredSoftBevelBorder();
        setBorder(BorderFactory.createCompoundBorder(outerBorder,innerBorder));
    }

}
