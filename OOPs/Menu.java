package OOPs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Menu extends JFrame {
    private ImageIcon image;
    private JLabel picLabel;
    private JButton foodBtn;


    public Menu() {
        super("Food Ordering System");
        setLayout(new BorderLayout());

        //setting the background image to appear
        image = new ImageIcon(getClass().getResource("food.jpg"));
        picLabel = new JLabel(image);

        //makes the "Order" Button. When pressed, disposes the current menu and opens the food menu
        foodBtn = new JButton("Order");
        foodBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new FoodMenu();
                dispose();
            }
        });

        add(picLabel,BorderLayout.CENTER);
        add(foodBtn, BorderLayout.SOUTH);


        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);

    }
}
